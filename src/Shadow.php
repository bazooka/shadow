<?php

namespace Fantassin\Shadow;

use Fantassin\Core\WordPress\HasHooks;
use Timber\Twig_Function;
use Twig\Environment;

/**
 * Create an obfuscation standalone Class
 */
class Shadow {
	/**
	 * @var string
	 */
	private $attribute;

	/**
	 * @param string $attribute
	 */
	public function __construct( string $attribute = 'data-shdw' ) {
		$this->attribute = $attribute;
	}

	/**
	 * @param $url
	 *
	 * @return string
	 */
	public function encode( $url ) {
		return base64_encode( str_rot13( $url ) );
	}

	/**
	 * @param $url
	 *
	 * @return string
	 */
	public function decode( $url ) {
		return base64_decode( str_rot13( $url ) );
	}

	/**
	 * Generate attributes
	 *
	 * @param string $url
	 * @param string $target
	 *
	 * @return string
	 */
	function get_attributes( string $url, string $target = null ): string {
		$res = '';

		if ( ! empty( $url ) ) {
			$res .= $this->attribute . '="' . $this->encode( $url ) . '"';
		}

		if ( ! is_null( $target ) ) {
			$res .= ' ' . $this->attribute . '-target="' . $target . '"';
		}

		return $res;
	}
}
