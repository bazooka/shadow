<?php

namespace Fantassin\Shadow;

use Fantassin\Core\WordPress\HasHooks;
use Timber\Twig_Function;
use Twig\Environment;

/**
 * Create an obfuscation function for Twig
 */
class TimberShadow implements HasHooks {

	public function hooks() {
		add_filter( 'timber/twig', [ $this, 'add_to_twig' ] );
	}

	/**
	 * @param $twig
	 *
	 * @return Environment
	 */
	public function add_to_twig( $twig ) {
		$shadow = new Shadow( apply_filters( 'fantassin.shadow.attribute', 'data-shdw' ) );
		$twig->addFunction(
			new Twig_Function(
				'shadow',
				function ( $url, $target = null ) use ( $shadow ) {
					return $shadow->get_attributes( $url, $target );
				}
			)
		);

		return $twig;
	}
}
