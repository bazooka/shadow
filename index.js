export default class Shadow {

    constructor(attr) {
        this.selector = attr;
        this.hrefs = Array.from(document.querySelectorAll('[' + this.selector + ']'))

        if (!this.hrefs) {
            return
        }

        this.init = this.init.bind(this);
    }

    init() {
        for (let i = 0; i < this.hrefs.length; i++) {
            let shadowElement = new ShadowElement(this.selector, this.hrefs[i])
            shadowElement.init();
        }
    }
}

export class ShadowElement {

    constructor(attribute, element) {
        this.attribute = attribute
        this.element = element

        if (!this.element) {
            return
        }

        this.handle = this.handle.bind(this)
        this.decode = this.decode.bind(this)
    }

    init() {
        this.element.addEventListener('click', this.handle)
        this.element.addEventListener('keypress', this.handle)
    }

    handle(e) {
        // During keypress event, redirect only on enter key
        if (e.type === "keypress" && e.keyCode !== 13) {
            return
        }

        e.preventDefault()

        // TODO: redirect vers admin-post.php qui decode base64
        const decodedLink = this.decode()
        const attribute = this.element.getAttribute(`${this.attribute}-target`);

        if (attribute === "_blank" || e.shiftKey || e.ctrlKey || e.metaKey) {
            this.redirectBlank(decodedLink)
        } else {
            this.redirect(decodedLink)
        }
    }

    redirect(decodedLink) {
        document.location.href = decodedLink;
    }

    redirectBlank(decodedLink) {
        window.open(decodedLink, '_blank');
    }

    strRot13(str) {
        return (str + '').replace(/[a-z]/gi, function (s) {
            return String.fromCharCode(s.charCodeAt(0) + (s.toLowerCase() < 'n' ? 13 : -13));
        });
    }

    decode() {
        let value = this.element.getAttribute(this.attribute)
        return decodeURIComponent(this.strRot13(window.atob(value)))
    }
}
