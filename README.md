If you want some internal linking optimization, you can obfuscate links with this library.

## 📦 How to install this project ?
```bash
composer require fantassin/shadow
npm install @fantassin/shadow
```

## ⚡ How it works ?
To generate obfuscate links you can use standalone `Shadow` class like this in your PHP file :
```php
<?php

use Fantassin\Shadow\Shadow;

$shadow = new Shadow();
// Get encoded version of link.
$shadow->encode( 'https://www.fantassin.fr' ); // dWdnY2Y6Ly9qamouc25hZ25mZnZhLnNl

// Or you can get attributes generated automatically
echo '<span ' . $shadow->get_attributes( 'https://www.fantassin.fr' ) . '>Your anchor link</span>';

// And you will get :
// <span data-shdw="dWdnY2Y6Ly9qamouc25hZ25mZnZhLnNl">Your anchor link</span>
```

## 🎯 Advanced usage
##### 1. Customize attributes
```php
<?php

use Fantassin\Shadow\Shadow;

$shadow = new Shadow( 'your-attribute' );
// Get customize attributes
echo '<span ' . $shadow->get_attributes( 'https://www.fantassin.fr' ) . '>Your anchor link</span>';

// And you will get :
// <span your-attribute="dWdnY2Y6Ly9qamouc25hZ25mZnZhLnNl">Your anchor link</span>
```
##### 2. Use target attribute
```php
<?php

use Fantassin\Shadow\Shadow;

$shadow = new Shadow();
// Or you can get attributes generated automatically
echo '<span ' . $shadow->get_attributes( 'https://www.fantassin.fr', '_blank' ) . '>Your anchor link</span>';

// And you will get :
// <span data-shdw="dWdnY2Y6Ly9qamouc25hZ25mZnZhLnNl" data-shadow-target="_blank">Your anchor link</span>

$shadow = new Shadow( 'your-attribute' );
// Get customize attributes
echo '<span ' . $shadow->get_attributes( 'https://www.fantassin.fr', '_blank' ) . '>Your anchor link</span>';

// And you will get :
// <span your-attribute="dWdnY2Y6Ly9qamouc25hZ25mZnZhLnNl" your-attribute-target="_blank">Your anchor link</span>
```
